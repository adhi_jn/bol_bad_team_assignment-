/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bol_bad_teamassignment;

/**
 *
 * @author Adhi Joyo Negoro
 */
public class tugas_kelompok_ke_2_nomor_1 {
//overloading perbedaan jumlah data parameter

    static void vaTest(int... no) {
        System.out.print("vaTest(int ...): "
                + "Number of args: " + no.length + " Contents: ");
        for (int n : no) {
            System.out.print(n + " ");
        }
        System.out.println();
    }
//overloading perbedaan tipe data parameter

    static void vaTest(boolean... bl) {
        System.out.print("vaTest(boolean ...) "
                + "Number of args: " + bl.length + " Contents: ");
        for (boolean b : bl) {
            System.out.print(b + " ");
        }
        System.out.println();
    }
    //overloading perbedaan urutan tipe data parameter

    static void vaTest(String msg, int... no) {
        System.out.print("vaTest(String, int ...): "
                + msg + "no. of arguments: " + no.length + " Contents: ");
        for (int n : no) {
            System.out.print(n + " ");
        }
        System.out.println();
    }

    static void vaTest(String msg, boolean... bl) {
        System.out.print("vaTest(String, Boolean ...): "
                + msg + " no. of arguments: " + bl.length + " Contents: ");
        for (boolean b : bl) {
            System.out.print(b + " ");
        }
        System.out.println();
    }

    public static void main(String args[]) {
        vaTest(1, 2, 3, 4, 5, 6);
        vaTest("Testing: ", 10, 20);
        vaTest(true, false, false);
        vaTest("Checking", true, false);
    }
}

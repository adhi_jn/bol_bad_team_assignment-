/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bol_bad_teamassignment;

/**
 *
 * @author Adhi Joyo Negoro
 */
public class tugas_kelompok_ke_1_nomor_1_soal_b {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Reverse();
    }
    private final static int MAX_LENGTH1 = 24;

    private static String buffer1 = "";

    private static void emit1(String nextChunk1) {
        if (buffer1.length() + nextChunk1.length() > MAX_LENGTH1) {
            System.out.println(buffer1);
            buffer1 = "";
        }
        if (nextChunk1.length() == 2) {
            nextChunk1 = " 0" + nextChunk1.trim();
        }
        buffer1 = nextChunk1 + buffer1;
    }

    private static final int N = 100;
    private static void Reverse() {
        for (int count1 = 2; count1 <= N; count1 = count1 + 2) {
            emit1(" " + count1);
        }
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bol_bad_teamassignment;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.text.*;
import javax.swing.*;

/**
 *
 * @author Adhi Joyo Negoro
 */
public class tugas_kelompok_ke_4_nomor_1 {

    public static void main(String[] argv) {
        JFrame frame = new JFrame();
        frame.getContentPane().add(new MyComponent());
        frame.setSize(612, 612);
        frame.setVisible(true);
    }
}

class Slice {

    double value;
    Color color;

    public Slice(double value, Color color) {
        this.value = value;
        this.color = color;
    }
}

class MyComponent extends JPanel {

    NumberFormat numberFormat;
    Slice[] slices = {
        new Slice(5, Color.black),
        new Slice(33, Color.green),
        new Slice(20, Color.blue),
        new Slice(15, Color.red)
    };

    public MyComponent() {
        numberFormat = NumberFormat.getPercentInstance();
    }

    public void paint(Graphics g) {
        drawPie((Graphics2D) g, getBounds(), slices);
    }

    void drawPie(Graphics2D g, Rectangle area, Slice[] slices) {
        double total = 0.0D;
        int koordinatY = 40;

        for (int i = 0; i < slices.length; i++) {
            total += slices[i].value;
        }
        double curValue = 0.0D;
        int startAngle = 0;
        for (int i = 0; i < slices.length; i++) {
            startAngle = (int) (curValue * 360 / total);
            int arcAngle = (int) (slices[i].value * 360 / total);
            g.setColor(slices[i].color);
            g.fillArc(area.x, area.y, area.width, area.height, startAngle, arcAngle);
            curValue += slices[i].value;
            String st = numberFormat.format(slices[i].value / total);
            g.drawString(st, 100, koordinatY);
            koordinatY = koordinatY + 15;
        }
    }
}

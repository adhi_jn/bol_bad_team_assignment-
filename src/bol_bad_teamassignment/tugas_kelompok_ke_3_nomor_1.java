/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bol_bad_teamassignment;

/**
 *
 * @author Adhi Joyo Negoro
 */
public class tugas_kelompok_ke_3_nomor_1 {

    static Node lList;

    static class Node {

        int isi;
        Node next;

        Node(int i) {
            isi = i;
            next = null;
        }
    }

    static void sisip(int x) {
        if (lList == null) {
            lList = new Node(x);
        } else {
            Node newNode = new Node(x);
            Node pointer = lList;
            int length = 0;
            while (pointer != null) {
                length++;
                pointer = pointer.next;
            }

            int jumlah = ((length % 2) == 0) ? (length / 2) : (length + 1) / 2;
            pointer = lList;

            while (jumlah-- > 1) {
                pointer = pointer.next;
            }
            newNode.next = pointer.next;
            pointer.next = newNode;
        }
    }

    static void cetak() {
        Node temporary = lList;
        while (temporary != null) {
            System.out.print(temporary.isi + " ");
            temporary = temporary.next;
        }
        System.out.println();
    }

    public static void main(String[] args) {
        lList = null;
        lList = new Node(0);
        lList.next = new Node(1);
        lList.next.next = new Node(2);
        lList.next.next.next = new Node(3);
        lList.next.next.next.next = new Node(4);
        lList.next.next.next.next.next = new Node(5);
        lList.next.next.next.next.next.next = new Node(6);
        cetak();
        int x = 99;
        sisip(x);
        cetak();
    }

}

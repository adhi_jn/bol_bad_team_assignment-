/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bol_bad_teamassignment;

/**
 *
 * @author Adhi Joyo Negoro
 */
public class tugas_kelompok_ke_1_nomor_1_soal_a {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        countTo_N_Improved();
    }
    private final static int MAX_LENGTH = 24;

    private static String buffer = "";

    private static void emit(String nextChunk) {
        if (buffer.length() + nextChunk.length() > MAX_LENGTH) {
            System.out.println(buffer);
            buffer = "";
        }
        if (nextChunk.length() == 2) {
            nextChunk = " 0" + nextChunk.trim();
        }
        buffer += nextChunk;
    }
    private static final int N = 100;

    private static void countTo_N_Improved() {
        for (int count = 2; count <= N; count = count + 2) {
            emit(" " + count);
        }
    }
}

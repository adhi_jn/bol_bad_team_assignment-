/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bol_bad_teamassignment;

import java.util.ArrayList;

/**
 *
 * @author Adhi Joyo Negoro
 */
public class tugas_kelompok_ke_1_nomor_2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        countTo_N_Improved();
    }
    private final static int MAX_LENGTH = 24;
    private static String buffer = "";
    private static int[] array = new int[6];
    private static int j = 0;

    private static void emit(String nextChunk) {
        if (buffer.length() + nextChunk.length() > MAX_LENGTH) {
            System.out.println(buffer + "= " + String.valueOf(array[j]));
            buffer = "";
        }
        array[j] += Integer.parseInt(nextChunk.trim());
        buffer = nextChunk + buffer;
    }
    private static final int N = 100;

    private static void countTo_N_Improved() {
        for (int count = 2; count <= N; count = count + 2) {
            if (count < 10) {
                emit("0" + count + " ");
            } else {
                emit(count + " ");
            }
        }
    }
}
